# MontiArc VSCode

This VSCode extension provides language support for MontiArc (arc) files.

## Features

Currently the extension provides the following features:

* syntax highlighting
* syntax highlighting of inline Java code
* snippets
* code folding


## Known Issues

Currently no language server is used.

## Release Notes

This extension is currently in beta

-----------------------------------------------------------------------------------------------------------

## Development Information

* The tmLanguage syntax is generated using [Iro](https://eeyo.io/iro/)
* For the conversion of the TextMate grammar to JSON we use this [website](http://json2plist.sinaapp.com/)

**IMPORTANT: After the generation and conversion of the grammar change set the javainline object as shown below to get java highlighting:**
```json
"javainline": {
    "patterns": [
        {
            "include": "source.java"
        }
    ]
},
```
